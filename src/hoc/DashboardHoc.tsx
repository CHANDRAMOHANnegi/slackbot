// @ts-ignore
import React, {Component} from 'react';
import DashboardSidebar from "../component/DashboardSidebar";

class DashboardHoc extends Component {
    render() {
        return (
            <div className="dashboard">
                <div className="sidebarContainer">
                    <DashboardSidebar/>
                </div>
                {this.props.children}
            </div>
        );
    }
}

export default DashboardHoc;