import gql from 'graphql-tag';

const FETCH_EMPLOYEES = gql`
   query{
       fetchEmployees{
           slackId,
           firstName,
           lastName,
           email,
           phone
       }
   }
`;


const FETCH_EMPLOYEES_CURRENT_MONTH_ATTENDANCE = gql`
    query{
  fetchEmpCurrentMonthAttendance{
    date
    exitTime
    entryTime
    onLeave
  }
}
`;


export {
    FETCH_EMPLOYEES,
    FETCH_EMPLOYEES_CURRENT_MONTH_ATTENDANCE
}