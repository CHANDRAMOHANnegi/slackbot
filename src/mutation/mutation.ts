import gql from 'graphql-tag';

const CREATE_ORG = gql`
   mutation createOrg($orgName:String!, $email:String!, $password:String!){
       createOrg(orgName:$orgName, email:$email, password:$password){
           token,
       }
   }
`;

const LOGIN = gql`
   mutation($email:String!, $password:String!){
        login(email:$email, password:$password){
            token,
        }
   }
`;


const CREATE_EMPLOYEES = gql`
    mutation($employeeData:[EmployeeInput!]!){
        createEmployees(employeeData:$employeeData)
    }
`;

const TEST_STRING = gql`
    query {
        testString
    }
`;

export {
    CREATE_ORG,
    LOGIN,
    CREATE_EMPLOYEES,
    TEST_STRING
}

//[firstName:$employeeData.firstName,lastName:$employeeData.lastName,
//         email:$employeeData.email,phone:$employeeData.phone,slackId:$employeeData.slackId]