// @ts-ignore
import React, {Component} from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import './App.css';
import SignUp from './component/container/SignUp';
import {ApolloProvider} from "react-apollo";
import ApolloClient from "apollo-boost";
import SignIn from "./component/container/SignIn";
import '../src/assets/scss/main.scss'
import Dashboard from "./component/container/Dashboard";
import {connect} from "react-redux";
import ILoginStateEnum from "./constants/ILoginStateEnum";

const client = new ApolloClient({
    uri: "http://localhost:4000/graphql",
    fetchOptions: {
        credentials: "include"
    },
    request: operation => {
        const token = localStorage.getItem('attendance-token');
        operation.setContext({
            headers: {
                'authorization-org': token
            }
        })
    }
});

interface IAppProps {
    loggedIn: ILoginStateEnum,
    currentUser: string | null
}

interface IAppState {
}

class App extends Component<IAppProps, IAppState> {
    render() {
        return (
            <ApolloProvider client={client}>
                {localStorage.getItem('attendance-token') || this.props.currentUser ?
                    (<Router>
                        <Route path='/dashboard' component={Dashboard}/>
                    </Router>)
                    :
                    (<Router>
                        <Route path={'/signup'} component={SignUp}/>
                        <Route exact path={'/'} component={SignIn}/>
                    </Router>)
                }
            </ApolloProvider>
        )
    }
}


const mapStateToProps = (state: any) => {
    return {
        loggedIn: state.loggedIn,
        currentUser: state.currentUser
    }
};

export default connect(mapStateToProps, null)(App);
