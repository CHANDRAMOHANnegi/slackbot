import ILoginStateEnum from "../../constants/ILoginStateEnum";
import IUser from "./IUser";

interface IInitialState {
    loggedIn: ILoginStateEnum;
    currentUser: IUser | null;
}

export default IInitialState;