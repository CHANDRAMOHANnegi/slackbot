import {Component} from 'react';
import {Dispatch} from "redux";
import IReduxAction from "../IReduxAction";
import ReduxActionType from "../ReduxActionType";
import ILoginStateEnum from "../../constants/ILoginStateEnum";

class UserAction extends Component {

    public static loginAction() {
        return async (dispatch: Dispatch<IReduxAction>) => {
            dispatch({
                type: ReduxActionType.LOGIN_ACTION,
                params: {loggedIn: ILoginStateEnum.LOGGEDIN_SUCCESS, currentUser: localStorage.getItem('attendance-token')}
            });
        }
    }
}


export default UserAction;