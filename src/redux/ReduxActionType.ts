enum ReduxActionType {
    LOGIN_ACTION = "login",
    LOGOUT_ACTION = "logout",
}

export default ReduxActionType;