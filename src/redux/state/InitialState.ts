import ILoginStateEnum from "../../constants/ILoginStateEnum";
import IInitialState from "../../models/interfaces/IInitialState";


const InitialState: IInitialState = {
    currentUser: null,
    loggedIn: !!localStorage.getItem('attendance-token') ? ILoginStateEnum.LOGGEDIN_SUCCESS : ILoginStateEnum.LOGIN_NOTATTEMPTED,
};

export default InitialState;