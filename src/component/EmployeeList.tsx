// @ts-ignore
import React, {Component} from 'react';
import {Query} from "react-apollo";
import {FETCH_EMPLOYEES} from "../queries/queries";
import {Link} from "react-router-dom";


interface IEmployeeListProps {
}

interface IEmployeeListState {
}

class EmployeeList extends Component<IEmployeeListProps, IEmployeeListState> {

    fetchEmployees = () =>
        (<Query query={FETCH_EMPLOYEES}>
                {({loading, error, data}: any) => {
                    if (loading) return "Loading...";
                    if (error) return `Error! ${error.message}`;
                    return (
                        <div className="container-fluid employeeList">
                            <table className="table table-hover">
                                <thead className="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">SlackId</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Phone</th>
                                </tr>
                                </thead>
                                <tbody>
                                {data.fetchEmployees && data.fetchEmployees.map((employee: any, i: number) => {
                                    return (
                                        <tr key={i}>
                                            <th scope="row">{i + 1}</th>
                                            <td>{employee.slackId}</td>
                                            <td><Link to={`/dashboard/calendar/${employee.slackId}`}>
                                                {employee.firstName + " " + employee.lastName}
                                            </Link>
                                            </td>
                                            <td>{employee.email}</td>
                                            <td>{employee.phone}</td>
                                        </tr>
                                    )
                                })}
                                </tbody>
                            </table>
                        </div>
                    );
                }}
            </Query>
        );

    render() {
        return (
            <div>
                {this.fetchEmployees()}
            </div>
        );
    }
}

export default EmployeeList;