import React, {Component} from 'react';
import {withRouter} from "react-router";

interface IDashboardNavProps {
    isLoggedInWithSlack: boolean
}

interface IDashboardNavState {
}

class DashboardNav extends Component<IDashboardNavProps, IDashboardNavState> {

    logout = () => {
        localStorage.clear();
        // @ts-ignore
        this.props.history.push('/');
    };

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <button className="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">

                        <li>
                            <form className="form-inline my-2 my-lg-0">
                                <input className="form-control mr-sm-2" type="search" placeholder="Search"
                                       aria-label="Search"/>
                                <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                            </form>
                        </li>

                        <li className="nav-item active">
                            <a className="nav-link" href="/dashboard">Home <span
                                className="sr-only">(current)</span></a>
                        </li>

                        <li className="nav-item">
                            <a className="nav-link " onClick={this.logout}>LogOut</a>
                        </li>

                        <li>
                            {!this.props.isLoggedInWithSlack &&
                            <button>
                                <a href="https://slack.com/oauth/authorize?scope=identity.basic&client_id=649830632706.657593518289"><img
                                    src="https://api.slack.com/img/sign_in_with_slack.png"/></a>
                            </button>}</li>
                    </ul>
                </div>
            </nav>
        );
    }
}

// @ts-ignore
export default withRouter(DashboardNav);
