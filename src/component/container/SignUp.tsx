// @ts-ignore
import React, {Component} from 'react'
import {Mutation} from 'react-apollo';
import {CREATE_ORG} from '../../mutation/mutation';

interface ISignUpProps {

}

interface ISignUpState {
    email: string;
    password: string;
    orgName: string
}

export default class SignUp extends Component<ISignUpProps, ISignUpState> {
    constructor(props: any) {
        super(props);
        this.state = {
            email: '',
            password: '',
            orgName: '',
        }
    }

    handleSubmit = async (e: React.SyntheticEvent, createOrg: any) => {
        e.preventDefault();

        try {
            const response = await createOrg({variables: {...this.state}});
            console.log(response, createOrg.__typename);
            localStorage.setItem("attendance-token", response.data.createOrg.token);
        } catch (e) {
            if (e.message.includes("duplicate key")) {
                console.log("Email already registered");
            }
            if (e.message.includes("Network error")) {
                console.log("Check your connection")
            }
        }
        this.setState({
            email: '',
            password: '',
            orgName: '',
        })
    };

    handleChange = (e: React.SyntheticEvent) => {
        // @ts-ignore
        const [name, value] = [e.target.name, e.target.value];
        this.setState({
            ...this.state,
            [name]: value,
        })


    };

    render() {
        const {orgName, email, password} = this.state;
        return (
            <Mutation mutation={CREATE_ORG}>
                {(createOrg: any, {loading, error, data}: any) => {
                    return (
                        <section className="forms container align-middle ">
                            <div className="formDiv ">

                                <form onSubmit={(e) => this.handleSubmit(e, createOrg)}>
                                    <h2 className="align-self-center">SignUp</h2>

                                    <label htmlFor="exampleInputEmail1">Email address</label>
                                    <input type="email" className="form-control" id="exampleInputEmail1"
                                           onChange={this.handleChange}
                                           name="orgName"
                                           aria-describedby="emailHelp"
                                           placeholder="Organization Name"/>

                                    <label htmlFor="exampleInputEmail1">Email address</label>
                                    <input type="email" className="form-control" id="exampleInputEmail1"
                                           onChange={this.handleChange}
                                           name="email"
                                           aria-describedby="emailHelp"
                                           placeholder="E-mail"/>

                                    <label htmlFor="exampleInputEmail1">Email address</label>
                                    <input type="password" className="form-control" id="exampleInputEmail1"
                                           onChange={this.handleChange}
                                           name="password"
                                           aria-describedby="emailHelp"
                                           placeholder="password"/>
                                    <button type="submit" className="btn btn-primary align-self-center ">SignIn</button>
                                </form>
                            </div>
                        </section>
                    )
                }
                }
            </Mutation>
        )
    }
}