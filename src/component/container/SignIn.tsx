// @ts-ignore
import React, {Component} from 'react';
import {LOGIN} from "../../mutation/mutation";
import {Mutation} from "react-apollo";
import IReduxAction from "../../redux/IReduxAction";
import {Dispatch} from "redux";
import UserAction from "../../redux/actions/UserAction";
import {connect} from "react-redux";

interface ISignInProps {

}

interface ISignInState {
    email: string;
    password: string;
}

class SignIn extends Component<ISignInProps, ISignInState> {
    constructor(props: any) {
        super(props);
        this.state = {
            email: 'cm@cm.cm',
            password: '',
        }
    }

    handleSubmit = async (e: React.SyntheticEvent, login: any) => {
        e.preventDefault();
        const {email, password} = this.state;

        if (!email || !password) {
            console.log("invalid details")
        } else {
            try {
                const response = await login({variables: {...this.state}});


                localStorage.setItem("attendance-token", response.data.login.token);

                // @ts-ignore
                this.props.loginUser();
                // @ts-ignore
                localStorage.getItem('attendance-token') && this.props.history.push('/dashboard')
            } catch (e) {
                if (e.message.includes("duplicate key")) {
                    console.log("Email already registered");
                }
                if (e.message.includes("Network error")) {
                    console.log("Check your connection")
                }
                if (e.message.includes("Invalid credentials")) {
                    console.log("Invalid credentials")
                }
            }
        }

    };

    handleChange = (e: React.SyntheticEvent) => {
        // @ts-ignore
        const [name, value] = [e.target.name, e.target.value];
        this.setState({
            ...this.state,
            [name]: value,
        })
    };

    render() {
        return (
            <Mutation mutation={LOGIN}>
                {(login: any) =>
                    <section className="forms container align-middle ">
                        <div className="formDiv ">
                            <form onSubmit={(e) => this.handleSubmit(e, login)}>
                                <div className="form-group">
                                    <label htmlFor="exampleInputEmail1">Email address</label>
                                    <input type="email" className="form-control" id="exampleInputEmail1"
                                           onChange={this.handleChange}
                                           name="email"
                                           aria-describedby="emailHelp"
                                           placeholder="Enter email"/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="exampleInputPassword1">Password</label>
                                    <input type="password" className="form-control" id="exampleInputPassword1"
                                           onChange={this.handleChange}
                                           placeholder="Password"
                                           name="password"/>
                                </div>
                                <button type="submit" className="btn btn-primary align-self-center ">SignIn</button>
                            </form>
                        </div>
                    </section>
                }
            </Mutation>
        );
    }
}

const mapDispatchToProps = (dispatch: Dispatch<IReduxAction>) => {
    return {
        loginUser: () => UserAction.loginAction()(dispatch),
    };
};


export default connect(null, mapDispatchToProps)(SignIn);