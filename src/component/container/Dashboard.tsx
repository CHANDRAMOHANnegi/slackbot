// @ts-ignore
import React, {Component} from 'react';
import {Mutation} from 'react-apollo';
import axios from 'axios';
import * as queryString from 'query-string';
import {CREATE_EMPLOYEES} from "../../mutation/mutation";
import {connect} from "react-redux";
import DashboardHoc from "../../hoc/DashboardHoc";
import {Route} from "react-router";
import BotList from "../BotList";
import EmployeeList from "../EmployeeList";
import DashboardNav from "../DashboardNav";
import AttCalendar from "../Calendar";
import Calendar from "../Calendar";


interface IDashBoardProps {

}

interface IDashBoardState {
    teamMembersFromSlack: {
        employees: Array<Object>;
        bots: Array<Object>
    };
    teamMembersFromDataBase: Array<{}>;
    isLoggedInWithSlack: boolean;
}

class Dashboard extends Component<IDashBoardProps, IDashBoardState> {

    constructor(props: any) {
        super(props);
        this.state = {
            teamMembersFromSlack: {
                employees: [],
                bots: []
            },
            teamMembersFromDataBase: [],
            isLoggedInWithSlack: false
        }
    }

    async componentDidMount() {
        // @ts-ignore
        const queryParams = this.props.location.search;
        const parsed = queryString.parse(queryParams);

        if (parsed.code) {
            localStorage.setItem("slack_code", parsed.code.toString());
        }

        if (queryParams) {
            const response = await axios.get(`https://slack.com/api/oauth.access?client_id=649830632706.657593518289
            &client_secret=81c5227e351380294c63e82a5e3558ec&code=${parsed.code}`);


            const teamResponse = await axios.get(`https://slack.com/api/users.list?token=xoxp-649830632706-661298059061-663961123744-913403bbc87099e4aa7a8bfc3774eeae`);

            const {members: teamMembers} = teamResponse.data;


            let filteredMembers: Array<{}> = [];
            let bots: Array<{}> = [];
            teamMembers.forEach((teamMembers: any) => {

                    const {name, profile, is_bot, id: slackId} = teamMembers;

                    if (name === 'slackbot' || !is_bot) {

                        const {real_name, phone, email} = profile;

                        if (real_name === '' || phone === '' || email === '') {
                            filteredMembers.push({real_name, email, phone, slackId});
                        } else {
                            const full_name = real_name.split(' ');
                            let firstName, lastName;
                            firstName = full_name[0];
                            if (full_name.length === 1) {
                                lastName = ''
                            } else if (full_name.length === 2) {
                                lastName = full_name[1]
                            } else if (full_name.length === 3) {
                                lastName = full_name[2]
                            }
                            filteredMembers.push({firstName, lastName, email, phone, slackId});
                        }
                    } else {
                        const {real_name, bot_id} = profile;
                        bots.push({real_name, bot_id});
                        this.setState({
                            ...this.state,
                            teamMembersFromSlack: {
                                ...this.state.teamMembersFromSlack,
                                employees: filteredMembers,
                                bots: bots,
                            },
                            isLoggedInWithSlack: true
                        })
                    }
                }
            )
        }
    }

    createEmployee = () => {
        return (<Mutation mutation={CREATE_EMPLOYEES}
                          variables={{employeeData: [...this.state.teamMembersFromSlack.employees]}}>
            {(createEmp: any) =>
                <section className="  container align-middle ">
                    <button
                        onClick={() => {
                            createEmp()
                        }}
                        className="btn btn-primary align-self-center ">Add new employees from slack to database
                    </button>
                </section>
            }
        </Mutation>)
    };


    render() {
        return (
            <section>
                <div className="dashboardContainer">
                    <DashboardHoc>
                        <div className="mainContainer">
                            <DashboardNav isLoggedInWithSlack={this.state.isLoggedInWithSlack}/>
                            <Route path="/dashboard/bots"
                                   render={() => <BotList bots={this.state.teamMembersFromSlack.bots}/>}/>
                            <Route path="/dashboard/employees"
                                   render={() => <EmployeeList/>}/>
                            <Route path="/dashboard/calendar"
                                   component={Calendar}/>
                        </div>
                    </DashboardHoc>
                </div>
            </section>
        );
    }
}


const mapStateToProps = (state: any) => {
    return {
        loggedIn: state.loggedIn,
    }
};


export default connect(mapStateToProps, null)(Dashboard);
