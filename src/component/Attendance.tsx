import React, {Component} from 'react';
import {Query} from "react-apollo";
import {FETCH_EMPLOYEES_CURRENT_MONTH_ATTENDANCE} from "../queries/queries";

interface IAttendanceState {
    attendanceData: Array<Object>;
}

interface IAttendanceProps {

}

class Attendance extends Component<IAttendanceProps, IAttendanceState> {

    state = {
        attendanceData: [{
            date: "2/9/19",
            entryTime: "11:00",
            exitTime: "7:15",
            onLeave: false
        }, {
            date: "11/9/19",
            entryTime: "10:45",
            exitTime: "6:00",
            onLeave: false
        }, {
            date: "22/9/19",
            entryTime: "",
            exitTime: "",
            onLeave: true
        }, {
            date: "12/9/19",
            entryTime: "11:45",
            exitTime: "7:09",
            onLeave: false
        }],
    };


    render() {
        return (
            <Query query={FETCH_EMPLOYEES_CURRENT_MONTH_ATTENDANCE}>
                {({loading, error, data}: any) => {
                    if (loading) return null;
                    if (error) return `Error! ${error}`;
                    console.log(data);
                    return (
                        <div>
                            <div>Name : VIVEK DUBEY</div>
                        </div>
                    );
                }}
            </Query>
        );
    }
}

export default Attendance;