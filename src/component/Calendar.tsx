import React from 'react';
import moment, {Moment} from 'moment';
import Attendance from "./Attendance";

interface ICalendarState {
    today: Moment;
    showMonthPopup: boolean;
    selectedDay: string;
    selectedMonth: string;
    showYearNav: boolean;
    selectedYear: string;
}

interface ICalendarProps {
    history: History

}

class Calendar extends React.Component<ICalendarProps, ICalendarState> {

    constructor(props: any) {
        super(props);

        this.state = {
            today: moment(),
            showMonthPopup: false,
            showYearNav: false,
            selectedDay: moment().format("D"),
            selectedMonth: moment().format("MMMM"),
            selectedYear: moment().format("YYYY"),
        }
    }

    weekdays = moment.weekdays(); //["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
    weekdaysShort = moment.weekdaysShort(); // ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
    months = moment.months();

    year = () => {
        return this.state.today.format("Y");
    };
    month = () => {
        return this.state.today.format("MMMM");
    };
    daysInMonth = () => {
        return this.state.today.daysInMonth();
    };
    currentDate = () => {
        return this.state.today.get("date");
    };
    currentDay = () => {
        return this.state.today.format("D");
    };

    firstDayOfMonth = () => {
        let today = this.state.today;
        return moment(today).startOf('month').format('d'); // Day of week 0...1..5...6

    };

    setMonth = (month: any) => {
        let monthNo = this.months.indexOf(month);
        let today = Object.assign({}, this.state.today);
        let particularMonthdateContext = moment(today).set('month', monthNo);
        this.setState({
            today: particularMonthdateContext,
            selectedMonth: particularMonthdateContext.format("MMMM"),
            selectedYear: particularMonthdateContext.format("YYYY")
        })
    };

    nextMonth = () => {
        let today = Object.assign({}, this.state.today);
        this.setMonth(moment(today).add(1, "month").format("MMMM"));
    };

    prevMonth = () => {
        let today = Object.assign({}, this.state.today);
        this.setMonth(moment(today).subtract(1, "month"));
    };

    onChangeMonth = () => {
        this.setState({
            showMonthPopup: !this.state.showMonthPopup,
        });
    };

    MonthNav = () => {
        return (
            <span
                className="label-month"
                onClick={() => this.onChangeMonth()}>
                {this.month()}
                {this.state.showMonthPopup &&
                <div className="month-popup">
                    {moment.months().map((month: string | any) => {
                        return (
                            <div key={month}>
                                <span>
                                    <i onClick={() => this.setMonth(month)}>{month}</i>
                                </span>
                            </div>
                        );
                    })}
                </div>}
            </span>
        );
    };

    showYearEditor = () => {
        this.setState({
            showYearNav: true
        });
    };

    setYear = (year: any) => {
        console.log(year)
        let today = Object.assign({}, this.state.today);
        let particularYearContext = moment(today).set("year", year);
        this.setState({
            today: particularYearContext,
            selectedYear: particularYearContext.format("YYYY")
        });
    };

    onKeyUpYear = (e: React.SyntheticEvent) => {
        // @ts-ignore
        if (e.which === 13 || e.which === 27) {
            // @ts-ignore
            this.setYear(e.target.value);
            this.setState({
                showYearNav: false
            });
        }
    };

    YearNav = () => {
        return (
            this.state.showYearNav ?
                <input
                    defaultValue={this.year()}
                    className="editor-year"
                    onKeyUp={(e) => this.onKeyUpYear(e)}
                    onChange={(e) => this.setYear(e.target.value)}
                    type="number"
                    placeholder="year"/>
                :
                <span
                    className="label-year"
                    onDoubleClick={() => {
                        this.showYearEditor()
                    }}>
                    {this.year()}
                </span>
        );
    };

    onDayClick = (e: React.SyntheticEvent, day: any) => {
        this.setState({
            selectedDay: day,
        });
    };

    render() {

        // @ts-ignore
        console.log(this.props.history.location.pathname);
        let weekdays = this.weekdaysShort.map((day) => {
            return (
                <td key={day} className="week-day">{day}</td>
            )
        });
        let blanks = [];

        for (let i = 0; i < parseInt(this.firstDayOfMonth()); i++) {
            blanks.push(
                <td key={i * 80} className="emptySlot">
                    {" "}
                </td>
            );
        }

        let daysInMonth = [];
        for (let d = 1; d <= this.daysInMonth(); d++) {
            let className = (d === parseInt(this.currentDay()) ? "day current-day" : "day");
            let selectedClass = (d === parseInt(this.state.selectedDay) ? " selected-day " : "");

            daysInMonth.push(
                <td key={d} className={className + selectedClass}>
                    <span onClick={(e) => {
                        this.onDayClick(e, d)
                    }}>
                         {d}
                    </span>
                </td>
            );
        }

        let totalSlots = [...blanks, ...daysInMonth];
        let rows: Array<{}> = [];
        let cells: Array<{}> = [];

        totalSlots.forEach((date, i) => {

            if ((i % 7) !== 0) {
                cells.push(date);
            } else {
                let insertRow = cells.slice();
                rows.push(insertRow);
                cells = [];
                cells.push(date);
            }
            if (i === totalSlots.length - 1) {
                let insertRow = cells.slice();
                rows.push(insertRow);
            }
        });

        let trElems = rows.map((d, i) => {
            return (
                <tr key={i * 100}>
                    {d}
                </tr>
            );
        });

        return (
            <div className="calendar-container">
                <Attendance/>
                <table className="calendar table">
                    <thead>
                    <tr className="calendar-header">
                        <td colSpan={3}>
                            <a href="#"> <i onClick={this.prevMonth}>
                                &larr;prev
                            </i></a>
                        </td>
                        <td colSpan={3}>
                            <this.MonthNav/>
                            {" "}
                            <this.YearNav/>
                        </td>
                        <td colSpan={1}>
                            <a href="#">
                                <i onClick={this.nextMonth}>
                                    next&rarr;
                                </i>
                            </a>
                        </td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr className="weekDays">
                        {weekdays}
                    </tr>
                    {trElems}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Calendar;
