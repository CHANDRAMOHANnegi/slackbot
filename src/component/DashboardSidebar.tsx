// @ts-ignore
import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {ApolloConsumer} from "react-apollo";
import {FETCH_EMPLOYEES} from "../queries/queries";

interface IDashboardSidebarProps {
}

interface IDashboardSidebarState {
    teamMembersFromDataBase: Array<{}>
}

class DashboardSidebar extends Component<IDashboardSidebarProps, IDashboardSidebarState> {
    state = {
        teamMembersFromDataBase: []
    }

    fetchEmployees = () => {
        return <ApolloConsumer>
            {client => (
                <div>
                    <button
                        onClick={async () => {
                            const {data} = await client.query({
                                query: FETCH_EMPLOYEES,
                            });
                            this.setState({
                                ...this.state,
                                teamMembersFromDataBase: data.fetchEmployees
                            })
                        }}>
                        fetch Employees from database
                    </button>
                </div>
            )}
        </ApolloConsumer>
    };

    render() {
        return (
            <div className="sidebar">
                <div className="card text-center ">
                    <div className="card-body">
                        <h5 className="card-title">Special title treatment</h5>
                        <p className="card-text">With supporting text below as a </p>
                    </div>
                </div>
                <ul className="list-group">
                    <li className="list-group-item "><Link to="/dashboard">Home</Link></li>
                    <li className="list-group-item"><Link to="/profile">My Profile</Link></li>
                    <li className="list-group-item"><Link to="/dashboard">Admin</Link></li>
                    <li className="list-group-item"><Link to="/dashboard/employees">Employee List</Link></li>
                    <li className="list-group-item"><Link to="/dashboard/bots">Bots</Link></li>
                    <li className="list-group-item"><Link to="/dashboard/calendar">Calendar</Link></li>
                    <li className="list-group-item"><Link to="/notifications">Notifications</Link></li>
                    <li className="list-group-item"><Link to="/account">Account</Link></li>
                </ul>
            </div>
        );
    }
}

export default DashboardSidebar;