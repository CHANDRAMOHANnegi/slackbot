// @ts-ignore
import React, {Component} from 'react';

interface IBotListState {
    bots: Array<{}>
}

interface IBotListProps {
    bots: Array<{}>
}

class BotList extends Component<IBotListProps, IBotListState> {


    state = {
        bots: []
    };

    componentDidMount(): void {
        this.setState({
            bots: this.props.bots
        })
    }

    render() {
        const {bots} = this.state;
        return (
            <div className="container-fluid employeeList">
                <table className="table">
                    <thead className="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">SlackId</th>
                        <th scope="col">Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    {bots && bots.map((bot: any, i: number) => {
                        return <tr>
                            <th scope="row">{i + 1}</th>
                            <td>{bot.bot_id}</td>
                            <td>{bot.real_name}</td>
                        </tr>
                    })}
                    </tbody>
                </table>

            </div>
        );
    }
}

export default BotList;

